## API Documentation

You can find the Swagger documentation for our API [here](http://localhost:8080/swagger-ui.html).

### Swagger UI

You can also explore our API interactively using Swagger UI:

[Swagger UI](http://localhost:8080/swagger-ui.html)