package com.example.demo.controllers;

import com.example.demo.Socks;
import com.example.demo.SocksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


import java.util.List;

@RestController
@RequestMapping("/api/v1/socks")
@Api(value = "Socks Management System", description = "Operations pertaining to Socks in Socks Management System")

public class SocksController {

    private final SocksService socksService;

    @Autowired
    public SocksController(SocksService socksService) {
        this.socksService = socksService;
    }

    @GetMapping
    @ApiOperation(value = "View a list of available socks", response = List.class)

    public ResponseEntity<List<Socks>> getSocks() {
        List<Socks> socks = socksService.getSocks();
        return ResponseEntity.ok(socks);
    }

    @GetMapping("/get")
    @ApiOperation(value = "View a list of available socks by colour and cottonPart", response = List.class)
    public ResponseEntity<Integer> getSocksByColor(
            @RequestParam String color,
            @RequestParam int cottonPart
    ) {
        int count = socksService.getCount(color, cottonPart);
        return ResponseEntity.ok(count);
    }

    @PostMapping("/income")
    @ApiOperation(value = "Add socks")

    public ResponseEntity<String> registerNewSocks(@RequestBody List<Socks> socksList) {
        try {
            socksService.addNewSocks(socksList);
            return ResponseEntity.ok("Удалось добавить приход");
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body("Параметры запроса отсутствуют или имеют некорректный формат");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Произошла ошибка, не зависящая от вызывающей стороны");
        }
    }

    @DeleteMapping("/{socksId}")
    @ApiOperation(value = "Delete socks")

    public ResponseEntity<String> deleteSocks(@PathVariable("socksId") Long socksId) {
        try {
            socksService.deleteSocks(socksId);
            return ResponseEntity.ok("Удаление носков прошло успешно");
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.badRequest().body("Носки с указанным идентификатором не найдены");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Произошла ошибка, не зависящая от вызывающей стороны");
        }
    }
}




