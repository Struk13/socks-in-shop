package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SocksService {

    private final SocksRepository socksRepository;

    @Autowired
    public SocksService(SocksRepository socksRepository) {
        this.socksRepository = socksRepository;
    }

    public List<Socks> getSocks() {
        return socksRepository.findAll();
    }

    public void addNewSocks(List<Socks> socksList) {
        socksRepository.saveAll(socksList);
    }

    public void deleteSocks(Long socksId) {
        socksRepository.deleteById(socksId);
    }

    public int getCount(String color, int cottonPart) {
        return socksRepository.countSocksByColorAndCottonPart(color, cottonPart);
    }
}



